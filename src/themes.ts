import { colors, createTheme } from "@mui/material";


export const theme = createTheme({
    palette: {
        mode: "light",
        background: {
            default: "#ffffff"
        }
    }
})


export const darkTheme = createTheme({
    palette: {
        mode: "dark",
        background: {
            default: "#000000"
        }
    }
})