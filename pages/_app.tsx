import { ThemeProvider } from '@mui/material'
import { useState } from 'react'
import { darkTheme, theme } from 'src/themes'
import '../styles/globals.css'



function MyApp({ Component, pageProps }) {
  const [isDark, setTheme] = useState(true)

  function switchTheme() {
    setTheme(!isDark)
    document.body.style.backgroundColor = (isDark ? darkTheme : theme).palette.background.default
  }

  return (
    <ThemeProvider theme={isDark ? darkTheme : theme}>
      <Component {...pageProps} switchTheme={switchTheme} />
    </ThemeProvider>
  )
}

export default MyApp
